#include "line.h"

#include <iostream>

using std::endl;
using std::cout;

// 对具体实现细节的成员类的定义，通过对象指针调用服务

class Line::LinePimpl {
public:
    LinePimpl(int x1, int y1, int x2, int y2)
        :_p1(x1, y1), _p2(x2, y2) {}

    void printLinePimpl() const {
        _p1.print();
        cout << "----->";
        _p2.print();
        cout << endl;
    }

    ~LinePimpl() {
        cout << "~LinePimpl()" << endl;
    }

private:
    class Point {
    public:
        Point(int x, int y):_ix(x), _iy(y){
            cout << "Point(int = 0, int = 0)" << endl;
        }

        void print() const {
            cout << "(" << _ix << "," << _iy << ")";
        }

        ~Point(){
            cout << "~Point()" << endl;
        }
    private:
        int _ix;
        int _iy;
    };

private:
    Point _p1;
    Point _p2;
};

// 主类构造函数的初始化，由于数据成员是指针，要申请内存
Line::Line(int x1, int y1, int x2, int y2)
    :_pimpl(new LinePimpl(x1, y1, x2, y2)){
    cout << "Line(int, int, int, int)" << endl;
}

// 主类的打印函数，实则通过指针调用成员类的打印函数
void Line::PrintLine() const {
    _pimpl->printLinePimpl();
}

// 主类执行析构函数，释放指针
Line::~Line() {
    cout << "~Line()" << endl;
    if(_pimpl) {
        delete _pimpl;
        _pimpl = nullptr;
    }
}
