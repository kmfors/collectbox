#ifndef _LINE_H_
#define _LINE_H_


class Line {

public:
    Line(int x1, int y1, int x2, int y2);

    void PrintLine() const ;

    ~Line();

    // 具体的实现细节全都封装在LinePimpl中
    class LinePimpl; // 声明类
                     
private:
    // 由成员类声明的一个对象指针，是私有的，指针具体的实现细节不对外提供
    // 符合动态库特性
    LinePimpl *_pimpl;

};


#endif
