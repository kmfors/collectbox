#include <iostream>

using std::endl;
using std::cout;


class Line {
public:
    Line(int x1, int y1, int x2, int y2)
        :_p1(x1, y1), _p2(x2, y2) {}

    void PrintLine() {
        _p1.getPrint();
        cout << "---->";
        _p2.getPrint();
        cout << endl;
    }

// 内部类只为所在类提供服务，具体实现细节需要隐藏，不对外提供服务
// 因此将内部类设置为私有，体现封装性。
private:
    class Point {
    public:
        Point(int x, int y):_ix(x), _iy(y){}

        void getPrint(){
            cout << "(" << _ix << "," << _iy << ")";
        }
    private:
        int _ix;
        int _iy;
    };

private:
    Point _p1;
    Point _p2;
};

void test() {
    Line line(1, 2, 3, 4);
    line.PrintLine();
}

int main(){
    test();
    return 0;
}
