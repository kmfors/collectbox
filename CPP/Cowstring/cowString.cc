#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class String {
public:
    //为了让指针指向真正数据的位置，每次申请完空间
    //之后需要将指针向后偏移4字节
    String() : _pstr(new char[5]() + 4) {
        cout << "String()" << endl;
        *(int*)(_pstr - 4) = 1;
    }

    String(const char* pstr) : _pstr(new char[strlen(pstr) + 5]() + 4) {
        cout << "String(const char *)" << endl;
        strcpy(_pstr, pstr);
        *(int*)(_pstr - 4) = 1;
    }

    //String s2 = s1;
    String(const String& rhs) : _pstr(rhs._pstr) {
        cout << "String(const String &)" << endl;
        ++*(int*)(_pstr - 4);
    }

    //String s3("world");
    //s3 = s1
    String& operator=(const String& rhs) {
        cout << "String &operator=(const String &)" << endl;
        if (this != &rhs) { //1、自复制 
            //2、释放左操作数
            if (--*(int*)(_pstr - 4) == 0) {
                delete[](_pstr - 4);
            }
            //3、浅拷贝
            _pstr = rhs._pstr;
            ++*(int*)(_pstr - 4);
        }

        return *this;//4、返回*this
    }

    //s3[0] = 'H'
    char& operator[](size_t idx) {
        if (idx < size()) {
            if (*(int*)(_pstr - 4) > 1) { //共享的
                char* ptmp = new char[size() + 5]() + 4;
                strcpy(ptmp, _pstr);
                --*(int*)(_pstr - 4);
                _pstr = ptmp;
                *(int*)(_pstr - 4) = 1;
            }
            return _pstr[idx];
        } else {
            static char nullchar = '\0';
            return nullchar;
        }
    }

    //s1
    ~String() {
        cout << "~String()" << endl;
        if (--*(int*)(_pstr - 4) == 0) {
            delete[](_pstr - 4);
        }
    }

    const char* c_str() const {
        return _pstr;
    }

    size_t size() const {
        return strlen(_pstr);
    }

    int getRefCount() const {
        return *(int*)(_pstr - 4);
    }

    friend std::ostream& operator<<(std::ostream& os, const String& rhs);

private:
    char* _pstr;
};

std::ostream& operator<<(std::ostream& os, const String& rhs) {
    if (rhs._pstr) {
        os << rhs._pstr;
    }
    return os;
}
void test() {
    String s1("hello");
    cout << "s1 = " << s1 << endl;
    cout << "s1.getRefCount() = " << s1.getRefCount() << endl;
    printf("s1's address: %p\n", s1.c_str());

    cout << endl;
    String s2 = s1;
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s1.getRefCount() = " << s1.getRefCount() << endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() << endl;
    printf("s1's address: %p\n", s1.c_str());
    printf("s2's address: %p\n", s2.c_str());

    cout << endl;
    String s3("world");
    cout << "s3 = " << s3 << endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() << endl;
    printf("s3's address: %p\n", s3.c_str());

    cout << endl << "执行s3 = s1" << endl;
    s3 = s1;
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    cout << "s1.getRefCount() = " << s1.getRefCount() << endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() << endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() << endl;
    printf("s1's address: %p\n", s1.c_str());
    printf("s2's address: %p\n", s2.c_str());
    printf("s3's address: %p\n", s3.c_str());

    cout << endl << "对s3[0]执行写操作" << endl;
    s3[0] = 'H';//char = char
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    cout << "s1.getRefCount() = " << s1.getRefCount() << endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() << endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() << endl;
    printf("s1's address: %p\n", s1.c_str());
    printf("s2's address: %p\n", s2.c_str());
    printf("s3's address: %p\n", s3.c_str());

    cout << endl << "读s1[0]执行读操作" << endl;
    cout << "s1[0] = " << s1[0] << endl;//cout << char
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    cout << "s3 = " << s3 << endl;
    cout << "s1.getRefCount() = " << s1.getRefCount() << endl;
    cout << "s2.getRefCount() = " << s2.getRefCount() << endl;
    cout << "s3.getRefCount() = " << s3.getRefCount() << endl;
    printf("s1's address: %p\n", s1.c_str());
    printf("s2's address: %p\n", s2.c_str());
    printf("s3's address: %p\n", s3.c_str());
}

int main(int argc, char** argv) {
    test();
    return 0;
}


