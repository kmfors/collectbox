#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

// 3、使用atexit函数 + 饿汉模式

class Singleton {
public:
    static Singleton *getInstance() {
        //多线程不安全的问题
        if(nullptr == _pInstance) {
            _pInstance = new Singleton();
            atexit(destroy);
        }
        return _pInstance;
    }

    static void destroy() {
        if(_pInstance) {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

private:
    Singleton() {
        cout << "Singleton()" << endl;
    }

    ~Singleton() {
        cout << "~Singleton()" << endl;
    }

private:
    static Singleton *_pInstance;
};

/* Singleton *Singleton::_pInstance = nullptr; //饱（懒）汉模式*/
/* Singleton *Singleton::_pInstance = Singleton::getInstance();//饿汉模式 */
Singleton *Singleton::_pInstance = getInstance();//饿汉模式

int main(int argc, char **argv) {
    Singleton *ps = Singleton::getInstance();

    return 0;
}


