#include <iostream>

using std::cout;
using std::endl;

//1、单例模式自动释放的第一种形式：友元类
/* class AutoRelease;//类的前向声明 */

class Singleton {
    /* friend  AutoRelease; */
    friend class AutoRelease;
public:
    static Singleton *getInstance() {
        if(nullptr == _pInstance) {
            _pInstance = new Singleton();
        }
        return _pInstance;
    }

    static void destroy() {
        if(_pInstance) {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

private:
    Singleton() {
        cout << "Singleton()" << endl;
    }

    ~Singleton() {
        cout << "~Singleton()" << endl;
    }

private:
    static Singleton *_pInstance;
};

Singleton *Singleton::_pInstance = nullptr;

class AutoRelease {
public:
    AutoRelease() {
        cout << "AutoRelease()" << endl;
    }

    ~AutoRelease() {
        cout << "~AutoRelease()" << endl;
        if(Singleton::_pInstance) {
            delete Singleton::_pInstance;
            Singleton::_pInstance = nullptr;
        }
    }
};

int main(int argc, char **argv) {
    // 申请内存空间
    Singleton *ps = Singleton::getInstance();

    // 脱离main作用域后自动释放
    AutoRelease ar;//栈对象
    /* ps->destroy(); */

    return 0;
}


