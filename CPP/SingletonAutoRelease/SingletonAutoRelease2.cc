#include <iostream>

using std::cout;
using std::endl;

//2、单例模式自动释放的第2种形式：内部类 + 静态数据成员

class Singleton {
public:
    static Singleton *getInstance() {
        if(nullptr == _pInstance) {
            _pInstance = new Singleton();
            /* static AutoRelease ar;//ok */
        }
        return _pInstance;
    }

    static void destroy() {
        if(_pInstance) {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

private:
    Singleton() {
        cout << "Singleton()" << endl;
    }

    ~Singleton() {
        cout << "~Singleton()" << endl;
    }

private:
    //内部类
    class AutoRelease {
    public:
        AutoRelease() {
            cout << "AutoRelease()" << endl;
        }
    
        ~AutoRelease() {
            cout << "~AutoRelease()" << endl;
            if(_pInstance) {
                delete _pInstance;
                _pInstance = nullptr;
            }
        }
    };
    
private:
    static Singleton *_pInstance;
    static AutoRelease _ar;
};

/* Singleton *Singleton::_pInstance = nullptr; */
Singleton *Singleton::_pInstance = getInstance();
Singleton::AutoRelease Singleton::_ar;//

int main(int argc, char **argv) {
    Singleton *ps = Singleton::getInstance();

    /* AutoRelease ar;//栈对象,error */
    /* ps->destroy(); */

    return 0;
}


