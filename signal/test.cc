#include <iostream>
#include <string>

class Person {
public:

    //有参构造函数
    Person(std::string name, int age):_Name(name),_Age(age) {}

    //拷贝构造函数
    Person(const Person &rhs) :_Name(rhs._Name) ,_Age(rhs._Age) {
        std::cout << "Execute Copy_Call_Function " << std::endl;
    }

    void print(){
        std::cout << _Name << " : " << _Age << std::endl;
    }
public:
    std::string _Name;
    int _Age;
};

void test() {
    Person p1 = Person("Ellen",21);
    Person p2 = Person(p1);	//执行拷贝调用函数，将p1的数据拷贝到p2
    //Person p2 = p1; 也适用
    p2.print();

}


int main(void) {
    test();
    std::cout << "--------- end ----------" << std::endl;
    return 0;
}

