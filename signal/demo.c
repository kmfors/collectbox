#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>


void display(){
    printf("Received SIGINT signal. Exiting...\n");
    exit(0);
}

void test(){

    // 注册SIGINT信号处理函数
    signal(SIGINT,display);

    // 无限循环
    while(1){

    }

}

void thread_func(void* arg){

    sigset_t mask;

    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    pthread_sigmask(SIG_BLOCK, &mask, NULL);

   
}


int main(){

    pthread_t th1;
    pthread_create(&th1, NULL, thread_func, NULL);

    pthread_join(th1, NULL);
    test();
    return 0;
}
