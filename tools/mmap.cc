#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>
#include <unordered_map>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#endif

using namespace std;

unordered_map<string, string> g_map_Msg;
const char* src_path = "source.log";

class SubMsgOP {
public:
    // 加载文件
    static int InitMsg() {
        char* file_data = nullptr;
        size_t file_size = 0;

#ifdef _WIN32
        // 打开文件，没有就创建
        HANDLE hFile = CreateFileA(src_path, GENERIC_READ,
            FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile == INVALID_HANDLE_VALUE) {
            cerr << "Failed to open file." << endl;
            return -1;
        }
        // 获取文件大小
        DWORD w_size = GetFileSize(hFile, NULL);
        if (w_size == 0) {
            CloseHandle(hFile);
            return 0;
        }
        // 创建文件映射对象
        HANDLE hFileMapping = CreateFileMappingA(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
        if (hFileMapping == NULL) {
            cerr << "Failed to create file mapping object. " << endl;
            CloseHandle(hFile);
            return -1;
        }
        // 映射文件到进程地址空间
        LPVOID pMappedData = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
        if (pMappedData == NULL) {
            cerr << "Failed to map view of file. " << endl;
            CloseHandle(hFileMapping);
            CloseHandle(hFile);
            return -1;
        }
        
        // 操作内存映射数据
        file_data = static_cast<char*>(pMappedData);
        file_size = w_size;

#else
        int fd = open(src_path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        if (fd == -1) {
            cerr << "open error!" << endl;
            return -1;
        }
        off_t l_size = lseek(fd, 0, SEEK_END);  // 获取文件大小
        if (l_size == -1) {
            cerr << "file size lseek error!" << endl;
            close(fd);
            return -1;
        }
        else if (l_size == 0) {
            close(fd);
            return 0;
        }

        // 映射文件到内存
        void* temp_data = mmap(NULL, l_size, PROT_READ, MAP_SHARED, fd, 0);
        file_data = static_cast<char*>(temp_data);
        file_size = l_size;
#endif
        istringstream content(string(file_data, file_size));
        string line;
        while (std::getline(content, line)) {
            if (line.empty())    continue;
            istringstream line_data(line);
            string word;
            vector<string> vect;
            while (line_data >> word) {
                vect.push_back(word);
            }
            vector<string>::iterator iter = vect.begin();
            for (word.clear(); iter != std::prev(vect.end()); iter++) {
                word.append(*iter).append(" ");
            }
            g_map_Msg.emplace(word, *std::prev(vect.end()));
        }
#ifdef _WIN32
        UnmapViewOfFile(pMappedData);
        CloseHandle(hFileMapping);
        CloseHandle(hFile);

#else
        close(fd);
#endif
        return 0;
    }
    //-------------------------------------------
    // 打印容器信息
    static void display() {
        cout << "打印容器信息: " << endl;
        for (auto& iter : g_map_Msg) {
            cout << iter.first << iter.second << endl;
        }
    }

    // 写入本地文件
    static int SaveCache() {
        string allMsg;
        for (auto& iter : g_map_Msg) {
            allMsg.append(iter.first).append(iter.second).append("\n");
        }
        size_t len = strlen(allMsg.c_str());
#ifdef _WIN32
        // 打开文件，没有就创建
        HANDLE hFile = CreateFileA(src_path, GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile == INVALID_HANDLE_VALUE) {
            cerr << "Failed to open file." << endl;
            return -1;
        }
        // 获取文件大小
        //DWORD w_size = GetFileSize(hFile, NULL);
        
        // 创建文件映射对象
        HANDLE hFileMapping = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 0, len, NULL);
        if (hFileMapping == NULL) {
            cerr << "Failed to create file mapping object. " << endl;
            CloseHandle(hFile);
            return -1;
        }
        // 映射文件到进程地址空间
        LPVOID pMappedData = MapViewOfFile(hFileMapping, FILE_MAP_ALL_ACCESS, 0, 0, 0);
        if (pMappedData == NULL) {
            cerr << "Failed to map view of file. " << endl;
            CloseHandle(hFileMapping);
            CloseHandle(hFile);
            return -1;
        }

        // 操作内存映射数据
        char* file_data = static_cast<char*>(pMappedData);
        std::memcpy(file_data, allMsg.c_str(), len);

        UnmapViewOfFile(pMappedData);
        CloseHandle(hFileMapping);
        CloseHandle(hFile);
#else
        // 打开文件，没有就创建
        int fd = open(src_path, O_RDWR | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR);
        if (fd == -1) {
            cerr << "open error!" << endl;
            return -1;
        }
        // 要缓存的消息大小大于分配的映射文件大小，重新分配映射文件大小
        if (-1 == ftruncate(fd, static_cast<off_t>(len))) {
            cerr << "resize ftruncate error" << endl;
            return -1;
        }
        void* temp_data = mmap(NULL, len, PROT_WRITE, MAP_SHARED, fd, 0);
        char* file_data = static_cast<char*>(temp_data);

        // 写入文件
        std::memcpy(file_data, allMsg.c_str(), len);
        msync(static_cast<void*>(file_data), len, MS_ASYNC);
        close(fd);
#endif
        return 0;
    }

    // 添加至缓冲容器里
    static void AddToCache(string& key, string& value) {
        auto it = g_map_Msg.find(key);
        if (it == g_map_Msg.end()) {
            g_map_Msg.emplace(key, value);
        }
    }

};

int main() {
    SubMsgOP::InitMsg();
    SubMsgOP::display();
    string key("one two three four five six seven eight nine ");
    string value("zzz董");
    for (int i = 1; i < 10; ++i) {
        string tmp(to_string(i));
        tmp.append(key);
        SubMsgOP::AddToCache(tmp, value);
    }
    SubMsgOP::display();
    if (SubMsgOP::SaveCache()) {
        cerr << "save fail!" << endl;
    }
    else {
        cout << "save success!" << endl;
    }
}


