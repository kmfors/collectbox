#include <iostream>

/*
*   简单工厂模式：通过一个工厂类来封装产品对象的创建过程。客户端向工厂类传递不同的参数，由此来创建不同的具体产品对象。而创建不同具体产品的对象则基于类的多态，所有的具体产品类都继承抽象产品类从而调用共性的行为。
*
*   优势：将对象的创建逻辑集中到一个工厂类中，客户端代码只需要与工厂类进行交互，而不需要直接与具体对象进行交互。（客户端只知道传入工厂类的参数，对于如何创建的产品并不关心）
*/


// ----------------------------------------- 抽象产品 ---------------------------------------
class AbstractProductA {
public:
    virtual void Show() = 0;
};

class ProductA1 : public AbstractProductA {
public:
    virtual void Show() {
        std::cout << "I'm a ProductA1" << std::endl;
    }
};

class ProductA2 : public AbstractProductA {
public:
    virtual void Show() {
        std::cout << "I'm a ProductA2" << std::endl;
    }
};


class AbstractProductB {
public:
    virtual void Show() = 0;
};

class ProductB1 : public AbstractProductB {
public:
    virtual void Show() {
        std::cout << "I'm a ProductB1" << std::endl;
    }
};

class ProductB2 : public AbstractProductB {
public:
    virtual void Show() {
        std::cout << "I'm a ProductB2" << std::endl;
    }
};



// ----------------------------------------- 抽象工厂 ---------------------------------------

class AbstractFactory {
public:
    //AbstractFactory(){}

    virtual AbstractProductA* CreateProductA() = 0; 

    virtual AbstractProductB* CreateProductB() = 0;
};

class ConcreteFactory1 : public AbstractFactory {
public:
    virtual AbstractProductA* CreateProductA() {
        return new ProductA1();
    }
    virtual AbstractProductB* CreateProductB() {
        return new ProductB1();
    }
};

class ConcreteFactory2 : public AbstractFactory {
public:
    virtual AbstractProductA* CreateProductA() {
        return new ProductA2();
    }
    virtual AbstractProductB* CreateProductB() {
        return new ProductB2();
    }
};


int main() {
    AbstractFactory* factory1 = new ConcreteFactory1();
    AbstractProductA* productA1 = factory1->CreateProductA();
    AbstractProductB* productB1 = factory1->CreateProductB();

    productA1->Show();
    productB1->Show();

    delete productA1;
    delete productB1;
    delete factory1;

    AbstractFactory* factory2 = new ConcreteFactory2();
    AbstractProductA* productA2 = factory2->CreateProductA();
    AbstractProductB* productB2 = factory2->CreateProductB();

    productA2->Show();
    productB2->Show();

    delete productA2;
    delete productB2;
    delete factory2;
 

    return 0;
}