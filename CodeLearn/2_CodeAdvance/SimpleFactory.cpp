#include <iostream>
#include <string>

/*
*   简单工厂模式：通过一个工厂类来封装产品对象的创建过程。客户端向工厂类传递不同的参数，由此来创建不同的具体产品对象。而创建不同具体产品的对象则基于类的多态，所有的具体产品类都继承抽象产品类从而调用共性的行为。
*
*   优势：将对象的创建逻辑集中到一个工厂类中，客户端代码只需要与工厂类进行交互，而不需要直接与具体对象进行交互。（客户端只知道传入工厂类的参数，对于如何创建的产品并不关心）
*/
// 
class Products {
public:
    virtual void Show() = 0;
};

class ProductA : public Products {
public:
    virtual void Show() {
        std::cout << "I'm a ProductA" << std::endl;
    }
};

class ProductB : public Products {
public:
    virtual void Show() {
        std::cout << "I'm a ProductB" << std::endl;
    }
};

class SimpleFactory {
public:
    static Products* Create(const std::string& name) {
        if (name == "ProductA") {
            return new ProductA();
        } else if (name == "ProductB") {
            return new ProductB();
        } else {
            return nullptr;
        }
    }
};
int main() {
    Products* productA = SimpleFactory::Create("ProductA");
    Products* productB = SimpleFactory::Create("ProductB");

    productA->Show();
    productB->Show();

    delete productA;
    delete productB;

    return 0;
}